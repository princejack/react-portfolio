import React from 'react'
import Mynavbar from './components/NavbBar'
import MyCarousel from './components/Carousel'
import Banner from './pages/Banner'
import Portfolio from './pages/Portfolio'
import Contact from './pages/Contact'
import Footer from './pages/Footer'
import './App.css';

function App() {
  return (
    <div className="App">
      <Mynavbar />

      <Banner/>
      <Portfolio />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
