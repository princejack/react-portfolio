import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import './NavBar.css'

export default function MyNavBar() {
	return (
		<>
		<Navbar fixed="top" collapseOnSelect expand="md" bg="dark" variant="dark" className="animate-navbar nav-theme justify-content-between">
		  <Navbar.Brand href="#home">PJC</Navbar.Brand>
		  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  <Navbar.Collapse id="responsive-navbar-nav">
		    <Nav className="ml-auto">
		      <Nav.Link href="#home">Home</Nav.Link>
		      <Nav.Link href="#portfolioSection">Projects</Nav.Link>
		      <Nav.Link href="#contactSection">Contact</Nav.Link>
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
		</>
		)
}