import React from 'react'
import Profile from '../assets/profilepic.JPG'
import Image from 'react-bootstrap/Image'
import './BannerCss.css'

export default function Banner() {
	return(
		<>
			<div className="banner">
				<div className="banner-info">
					<Image className="profile justify-content-center mb-2" src={Profile} round />
					<h1 className="text-center">Prince Jack Chicano</h1>
					<p>Hi, I'm Prince Jack Chicano, aspiring Full-Stack Web Developer. I'm a committed individual, eager to learn, and dedicated to provide quality service.</p>


				</div>
			</div>
		</>
		)
}