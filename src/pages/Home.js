import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Profile from '../assets/profilepic.JPG'
import './HomeCss.css'



export default function Home(){
	return(
		<div id="home" className="mt-5 pt-5 pb-5">
			
			<Container fluid="md" className="myContainer">
				<Row>

					{/*ABOUT ME Description*/}
					<Col xs={12} md={6}>
						<Row className="align-items-start p-2 my-details rounded">
						{/*Description*/}
						<h1 className="text-white text-center myName">Prince Jack Chicano</h1>

						<p className="text-white homeDescription">Hi, I'm Prince Jack Chicano, aspiring Full-Stack Web Developer. I'm a committed individual, eager to learn, and dedicated to provide quality service.</p>
						
						</Row>
					</Col>

					{/*Profile pic*/}
					<Col xs={12} md={6} className="pl-5">
						<Row>
						<Image className="profile justify-content-center mb-2" src={Profile} />
						</Row>
					</Col>
					
				</Row>
			</Container>

		</div>
		)
}