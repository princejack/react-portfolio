import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import form from 'react-bootstrap/Form'
import './ContactCss.css'



export default function Contact(){
	return(
			<>
			<div id="contactSection">	
				<Container fluid="md" className="pt-5 pb-5">
					<Row>
						<Col>
							<h1 className="text-center pb-5">Get in touch</h1>
						</Col>
					</Row>
					<Row>
					<Col  xs={12} md={8}>	
						<div id="contact-form">	
						    <form>
					          <div className="form-group">
					            <label htmlFor="name">Name</label>
					            <input type="text" className="form-control"  />
					          </div>
					          <div className="form-group">
					            <label htmlFor="exampleInputEmail1">Email address</label>
					            <input type="email" className="form-control" aria-describedby="emailHelp"  />
					          </div>
					          <div className="form-group">
					            <label htmlFor="message">Message</label>
					            <textarea className="form-control" rows="5"  />
					          </div>
					          <button type="submit" className="btn btn-primary">Submit</button>
					        </form>
						</div>	
					</Col>
			      	</Row>
				</Container>
			</div>

			</>
		)
}