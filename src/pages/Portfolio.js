import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import capstone1 from '../assets/capstone1.png'
import capstone2 from '../assets/capstone2.PNG'
import capstone3 from '../assets/capstone3.PNG'
import './PortfolioCss.css'

export default function Portfolio(){

return (
		<>
		<div id="portfolioSection">
			<Container fluid >
				<Row>
					<Col>
					<h1 className="text-center mt-5">Portfolio</h1>
					</Col>
				</Row>

			</Container>
			<Container fluid>
				
				<Row>
					<div className="col-lg-4 col-sm-6">
						<a className="portfolio-box" href="#portfolioSection" >
							<img className="img-fluid p-3" src={capstone1} alt="" />
						</a>
							<div>
								<h4 className="text-center pb-1">Capstone 1: Landing Page</h4>
								<p className="text-center">The purpose of this project is to showcase my skills and the projects that i have built.</p>
								<p className="text-center  pb-5">Platforms used: HTML5, CSS3, BOOTSTRAP, Animate.css</p>
							</div>

					</div>
					<div className="col-lg-4 col-sm-6">
						<a className="portfolio-box" href="#portfolioSection" >
							<img className="img-fluid p-3" src={capstone2} alt="" />
						</a>	
							<div>
								<h4 className="text-center ">Capstone 2: Course Booking System</h4>
								<p className="text-center">The purpose of this project is to create an app wherein users can register, log in, and enroll courses.</p>
								<p className="text-center  pb-5">Platforms used: HTML5, CSS3, BOOTSTRAP, Node.js, Express, MongoDB</p>
							</div>
					</div>
					<div className="col-lg-4 col-sm-6">
						<a className="portfolio-box" href="#portfolioSection" >
							<img className="img-fluid p-3" src={capstone3} alt="" />
						</a>
							<div>
								<h4 className="text-center">Mini Capstone: Portfolio</h4>
								<p className="text-center">The purpose of this Dev Portfolio app was to showcase my skills and projects, using React.js as the platform
</p>
								<p className="text-center  pb-5">Platform used: React.Js</p>
							</div>
					</div>
				</Row>
			</Container>

		</div>
		</>
	)
}